# dubbo-admin

#### 介绍
官方dubbo-admin 已打jar包，可直接运行

或定位到dubbo-admin 修改配置文件，后自行编译

![输入图片说明](https://images.gitee.com/uploads/images/2021/1004/184405_f4a3f29d_2353571.png "屏幕截图.png")

编译
```
 mvn install -Dmaven.test.skip=true
```


启动dubbo-admin

![输入图片说明](https://images.gitee.com/uploads/images/2020/1231/164922_87460da8_2353571.png "屏幕截图.png")